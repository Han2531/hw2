## Homework 2


> Questions:
>- What kind of file did you just download?
  	1. SHA256 is a file without extension, I am not sure what it is
  	2. SHA256.asc should be an Action Script Communication File
  	3. multi.img.gz is a GNU zip (gzip) compression file

>- Why do we call it an "image"?

	Because it contains everything on the hard drive, bit by bit. It just likes the oranginal computer files

>- Why would Megi use this format to distribute it?
	
	Because it is huge, and a compression file could save a lot of space and downloading times. Also it includes many OS and different type of files. It is neat to compress them in a image file

#### [3 pts] .gitignore:


> Questions:
>   
>   - How do the output of the last two commands compare?

	The cat command have tow outputs for multi.img.gz and multi.img, whereas the SHA256sum only has one output line for multi.img.gz.

>   - Why would MEGI distribute the `SHA256` file with the image?  
> 	
	The SHA-256 could confirm both file integrity and authenticity.  It is good to run an SHA-256 hash comparison check when you install an operating system that has to be 100% correct.


#### [3 pts] Look at the Multiboot image (download file):

> Questions:
> 	- Why does it look funny?  (use `head` to look at this .md file for comparison)

	Because it is still a compressed file and 'head' is not ot read it correctly.


#### [6 pts] Extract the actual image from what you downloaded
	
> ##### Questions:
> - what does "`zcat`" mean (look it up)?

	It is a command to view the contents of a compressed file without uncompressing it

> - what happens after running this command?

	It runs for a while and then it shows the content of the compressed file.

> - what do the contents of the new file look like as compared to the *.gz version?

	It is still unreadable for me, although the two contents looks different.

#### [6 pts] Deeper inspection of image file
>Questions:
> - what do we know about the decompressed file `multi.img` ?

	It was 9.78 GiB,  10485760000 bytes, 20480000 sectors

> - what does it contain?

	Two images which are 196M and 9.6G 

> - how did the output of the `sha256sum` command compare to the info in the `SHA256` file?

	They are the same for `multi.img`, which means it is matched with the checksum file

#### [6 pts] Make the image look like a block-device to the system

>Questions:
>	- did we get a different response from `file` or `fdisk` when using the virtual (loopback) block device?

	Yes, it divide my loop0 into loop0p1 and loop0p2. Others remain the same

>	- is there anything suspicious about the output of either of those commands?

	Nothing suspicious to me


>	- What happened after we ran `losetup -P ...`??  Why??

	Nothing return in the terminal window, but from the manual, it indicates that the command forced the kernel to scan the partition table on the loop device

#### [6 pts] Filesystem info (again)

>Questions:
>	- did these commands both succeed?

	No, only part 2 was succeed. 

>	- If not, which one failed and why?

	The command for part 1 was failed due to wrong fs type, bad option, bad superblock on /dev/loop0p1, missing codepage or helper program, or other error. 

>	- what is the reason, do you think?

	I think it was failed because of wrong fs type.

#### [3 pts] Unmount the loopback devices

>Questions:
	- where does the _real_ file system on the loopback device reside?

	It should be in the hard drive of the host.

	- what happens to it after we unmount the device?

	It won't result in a loss of data or anything saved in the file. It simply tells my computer to stop showing things from it.
